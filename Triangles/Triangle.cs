﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Triangles
{
    public class Triangle
    {
        public static TriangleTypeEnum GetTriangleTypeFromSides(double a, double b, double c)
        {
            if (a >= b + c || b >= a + c || c >= a + b)
                throw new ArgumentException("It is not a triangle. The sum of lengths of two sides has to be more than length of the third side.");

            var sides = new[] { a, b, c };
            Array.Sort(sides);
            return Math.Pow(sides[0], 2) + Math.Pow(sides[1], 2) == Math.Pow(sides[2], 2) ? TriangleTypeEnum.RectangularTriangle
                : (Math.Pow(sides[0], 2) + Math.Pow(sides[1], 2) < Math.Pow(sides[2], 2) ? TriangleTypeEnum.ObtuseTriangle : TriangleTypeEnum.AcuteTriangle);
        }
    }

    public enum TriangleTypeEnum
    {
        AcuteTriangle, RectangularTriangle, ObtuseTriangle
    }
}
