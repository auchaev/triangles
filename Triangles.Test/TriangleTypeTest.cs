﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Triangles.Test
{
    [TestClass]
    public class TriangleTypeTest
    {
        [TestMethod]
        public void Can_calculate()
        {
            Assert.AreEqual(TriangleTypeEnum.AcuteTriangle, Triangle.GetTriangleTypeFromSides(3, 3, 2));
            Assert.AreEqual(TriangleTypeEnum.RectangularTriangle, Triangle.GetTriangleTypeFromSides(5, 4, 3));
            Assert.AreEqual(TriangleTypeEnum.ObtuseTriangle, Triangle.GetTriangleTypeFromSides(3, 5, 3));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Can_calculate_with_incorrect_sides()
        {
            Triangle.GetTriangleTypeFromSides(1, 2, 3);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Can_calculate_with_zero_side()
        {
            Triangle.GetTriangleTypeFromSides(1, 0, 3);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Can_calculate_with_negative_side()
        {
            Triangle.GetTriangleTypeFromSides(1, -1, 3);
        }
    }
}
